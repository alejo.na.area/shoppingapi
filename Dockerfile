## Build Stage

FROM golang:1.19-alpine AS build

WORKDIR /app

COPY . .

RUN go mod download

RUN go build -o /shoppingapi ./cmd/main.go

## Deploy Stage

FROM alpine:lastest

WORKDIR /app

COPY --from=build /shoppingapi /shoppingapi

EXPOSE 8080

CMD ["/shoppingapi"]