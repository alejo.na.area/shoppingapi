-- TABLES

CREATE TABLE IF NOT EXISTS users(
    id varchar(50) NOT NULL,
    name varchar(50) NOT NULL,
    lastname varchar(50) NOT NULL,
    dob TIMESTAMP NOT NULL,
    email varchar(100) NOT NULL,
    pw varchar(100) NOT NULL,
    gender varchar(10) NOT NULL,
    balance numeric(15,6) NOT NULL,

    CONSTRAINT pk_user PRIMARY KEY(id),
    UNIQUE(email)
);

CREATE TABLE IF NOT EXISTS transactions(
    id SERIAL,
    user_id varchar(50) NOT NULL,
    date TIMESTAMP NOT NULL,
    amount numeric(15,6) NOT NULL,
    operation varchar(10) NOT NULL,
    
    CONSTRAINT pk_transaction PRIMARY KEY(id),
    CONSTRAINT fk_user 
        FOREIGN KEY(user_id)
            REFERENCES users(id)
            ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS products(
    id SERIAL,
    name varchar(50) NOT NULL,
    price numeric(15,6) NOT NULL,
    qty int NOT NULL,
    
    CONSTRAINT pk_product PRIMARY KEY(name)
);
