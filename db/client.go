package db

import "database/sql"

func NewRelationalDB(dbHost string) (*sql.DB, error) {
	c, err := sql.Open("postgres", dbHost)
	if err != nil {
		return nil, err
	}
	return c, nil
}
