package handlers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/shoppingapi/pkg/core/domain"
	"gitlab.com/shoppingapi/pkg/utils"
)

//product http handler
/*
func (h *HTTPHandler) GetProduct(c *gin.Context) {

}
*/

func (h *HTTPHandler) GetAllProducts(c *gin.Context) {
	prods, err := h.prodService.ShowAllProducts()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Failed to reatrive all products", "error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, prods)
}

func (h *HTTPHandler) UploadProduct(c *gin.Context) {
	var prod domain.Product

	errp := c.BindJSON(&prod)
	if errp != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Failed to upload prod, JSON bind error", "error": errp.Error()})
		return
	}
	errv := prod.Validate()
	if errv != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Failed to upload prod, validate error", "error": errv.Error()})
		return
	}
	err := h.prodService.SaveProduct(prod)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Failed to upload product", "error": err.Error()})
		return
	}
}

/*
func (h *HTTPHandler) UploadManyProduct(c *gin.Context) {

}
*/
func (h *HTTPHandler) BuyProduct(c *gin.Context) {
	var id string
	//If the user is logged in (auth)
	user_id, errtok := idTokenCheck(c.GetHeader("token"))
	if errtok != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Session problem", "error": errtok.Error()})
		return
	}
	id = c.Query("id")
	if id == "" {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Product id not found"})
		return
	}
	log.Println("product id to buy is", id)
	err := h.prodService.BuyProduct(id, user_id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Cannot buy product", "error": err.Error()})
		return
	}

}

func (h *HTTPHandler) GetProductsXLM(c *gin.Context) {
	prods, err := h.prodService.ShowAllProducts()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Failed to reatrive the products to convert", "error": err.Error()})
		return
	}

	var pds domain.Products
	namefile := "products.xml"
	pds.Prods = prods

	file, errf := utils.CreateXML(pds)
	if errf != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Failed to Marshall the products", "error": errf.Error()})
	}

	errc := utils.CreateXMLfile(namefile, file)
	if errc != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Failed to create file", "error": errc.Error()})
	}

	zipname := "Products.zip"

	errz := utils.CreateZIPfile(namefile, zipname)
	if errz != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Failed to create zip", "error": errz.Error()})
	}

	c.XML(http.StatusOK, pds)

}
