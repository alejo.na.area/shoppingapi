package handlers

import (
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/shoppingapi/pkg/core/domain"
	"gitlab.com/shoppingapi/pkg/utils"
)

func (h *HTTPHandler) GetTransactions(c *gin.Context) {

	user_id, errtok := idTokenCheck(c.GetHeader("token"))
	if errtok != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Session problem", "error": errtok.Error()})
		return
	}

	transactions, err := h.txService.GetTransactions(user_id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error getting user transactions", "Error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, transactions)
}

func (h *HTTPHandler) Deposit(c *gin.Context) {
	var transaction domain.Operation

	user_id, errtok := idTokenCheck(c.GetHeader("token"))
	if errtok != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Session problem", "error": errtok.Error()})
		return
	}

	log.Println("Trying to make transaction")
	if err := c.BindJSON(&transaction); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}
	//validating deposit content
	errv := transaction.Validate()
	if errv != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": errv.Error()})
		return
	}

	errd := h.txService.MakeDeposit(user_id, transaction)
	if errd != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error making transaction", "Error": errd.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Deposit transaction was succesful"})

}

func (h *HTTPHandler) Withdraw(c *gin.Context) {
	var transaction domain.Operation

	user_id, errtok := idTokenCheck(c.GetHeader("token"))
	if errtok != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Session problem", "error": errtok.Error()})
		return
	}

	log.Println("Trying to make transaction")
	if err := c.BindJSON(&transaction); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}
	//validating withdraw content
	errv := transaction.Validate()
	if errv != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": errv.Error()})
		return
	}

	errw := h.txService.MakeWithdraw(user_id, transaction)
	if errw != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error making transaction", "Error": errw.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Withdraw transaction was succesful"})

}

//GetTransactionsXML

func (h *HTTPHandler) GetUserTransactionsXML(c *gin.Context) {

	user_id, errtok := idTokenCheck(c.GetHeader("token"))
	if errtok != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Session problem", "error": errtok.Error()})
		return
	}
	transactions, errtx := h.txService.GetTransactions(user_id)

	if errtx != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error getting user transactions", "Error": errtx.Error()})
		return
	}
	var txs domain.Transactions

	txs.Tx = transactions
	var str1, str2 strings.Builder
	str1.WriteString("user_")
	str2.WriteString("user_")
	str1.WriteString(user_id)
	str2.WriteString(user_id)
	str1.WriteString("_transactions")
	str2.WriteString("_transactions")

	str1.WriteString(".xml")
	namefile := str1.String()

	txsXML, errf := utils.CreateXML(txs)
	if errf != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Failed to Marshall the transactions", "error": errf.Error()})
	}

	errc := utils.CreateXMLfile(namefile, txsXML)
	if errc != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Failed to create file", "error": errc.Error()})
	}
	str2.WriteString(".zip")
	zipname := str2.String()
	errz := utils.CreateZIPfile(namefile, zipname)
	if errz != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Failed to create zip", "error": errz.Error()})
	}

	c.XML(http.StatusOK, txs)
}
