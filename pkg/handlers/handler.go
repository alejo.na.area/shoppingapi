package handlers

import (
	"errors"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/shoppingapi/pkg/core/domain"
	"gitlab.com/shoppingapi/pkg/core/ports"
)

type HTTPHandler struct {
	usersService ports.UsersService
	txService    ports.TransactionsService
	prodService  ports.ProductsService
}

func NewHTTPHandler(usvc ports.UsersService, tsvc ports.TransactionsService, psvc ports.ProductsService) *HTTPHandler {
	return &HTTPHandler{usersService: usvc,
		txService:   tsvc,
		prodService: psvc,
	}

}

func idTokenCheck(token string) (string, error) {
	var claims *domain.CustomClaims
	secret := "secretkey"
	tok, errt := jwt.ParseWithClaims(token, &domain.CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})
	if errt != nil {
		return "", errt
	}
	claims = tok.Claims.(*domain.CustomClaims)
	id := claims.ID
	if !tok.Valid {
		return id, errors.New("session no loger valid")
	}
	return id, nil
}
