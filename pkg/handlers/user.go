package handlers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/shoppingapi/pkg/core/domain"
)

func (h *HTTPHandler) GetUser(c *gin.Context) {
	game, err := h.usersService.ObtainUser(c.Param("id"))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}
	c.JSON(http.StatusOK, game)
}

func (h *HTTPHandler) Add(c *gin.Context) {
	var user domain.User
	log.Println("Trying to add user")
	if err := c.BindJSON(&user); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}
	//validate
	errv := user.Validate()
	if errv != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": errv.Error()})
		return
	}

	if err := h.usersService.AddUser(user); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}
	c.Status(http.StatusOK)
}

func (h *HTTPHandler) SearchUsers(c *gin.Context) {
	var q domain.QueryStruct
	q.Balance = (c.Query("balance"))
	q.Gender = (c.Query("gender"))
	q.Maxage = (c.Query("maxage"))
	q.Minage = (c.Query("minage"))
	errq := q.Validate()
	if errq != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": errq.Error()})
		return
	}
	games, err := h.usersService.SearchUsers(q)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}
	log.Println(games)
	c.JSON(http.StatusOK, games)
}

func (h *HTTPHandler) Checkpw(c *gin.Context) {
	var cred domain.Credentials
	if err := c.BindJSON(&cred); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}
	tok := h.usersService.CheckPass(cred)
	if tok == nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": tok})
		return
	}
	tokenString, errt := tok.SignedString([]byte("secretkey"))
	if errt != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "error to use secret key on the token"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Login succesful", "token": tokenString})
}

func (h *HTTPHandler) TokenStatus(c *gin.Context) {
	secret := "secretkey"
	token := c.GetHeader("token")
	tok, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Wrong token", "error": err.Error()})
		return
	}
	if !tok.Valid {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Invalid token"})
		return
	}
	log.Println("Valid token")
	c.JSON(http.StatusOK, gin.H{"message": "Valid session"})
}

func (h *HTTPHandler) Home(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "Welcome!"})
}
