package services

import (
	"errors"
	"log"
	"strconv"

	"gitlab.com/shoppingapi/pkg/core/domain"
)

//this part goes in the transactions folder

func (svc *services) GetTransactions(user string) ([]domain.Transaction, error) {

	transactions, err := svc.txsvc.txRepository.GetTransactions(user)
	if err != nil {
		log.Println("Transactions failed")
		return nil, err
	}
	log.Println("Transactions ok")
	return transactions, nil
}

func (svc *services) MakeDeposit(user string, operation domain.Operation) error {

	//update balance

	log.Println("Updating user balance")
	erru := svc.userscv.usersRepository.UpdateUserBalance(user, operation)
	if erru != nil {
		log.Println("Failed to deposit money while updating user balance")
		return erru
	} else {
		log.Println("Making transaction")
		err := svc.txsvc.txRepository.MakeTransaction(user, operation) // make transaction
		if err != nil {
			log.Println("Transactions failed")
			return err
		}
	}
	log.Println("Deposit was succesful")
	return nil
}

func (svc *services) MakeWithdraw(user string, operation domain.Operation) error {

	//**check user balance
	balance, errbal := svc.userscv.usersRepository.GetUserBalance(user)
	if errbal != nil {
		log.Println("Error getting user balance")
		return errbal
	}
	userBalance, _ := strconv.ParseFloat(balance, 64)
	withdrawAmt, _ := strconv.ParseFloat(operation.Amount, 64)

	if userBalance >= withdrawAmt {
		err := svc.txsvc.txRepository.MakeTransaction(user, operation) // make transaction
		if err != nil {
			log.Println("Transactions failed")
			return err
		}
		errupdt := svc.userscv.usersRepository.UpdateUserBalance(user, operation)
		if errupdt != nil {
			log.Println("Failed to withdraw money while updating user balance")
			return errupdt
		}
		log.Println("Withdraw was succesful")
		return nil
	} else {
		return errors.New("not enough balance in user account")
	}

}
