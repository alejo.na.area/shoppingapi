package services

import "gitlab.com/shoppingapi/pkg/core/ports"

type services struct {
	prodsvc *prodservice
	txsvc   *txservice
	userscv *userservice
}
type userservice struct {
	usersRepository ports.UsersRepository
}
type txservice struct {
	txRepository ports.TransactionsRepository
}
type prodservice struct {
	prodRepository ports.ProductsRepository
}

func NewService(prodRepo ports.ProductsRepository, txRepo ports.TransactionsRepository, userRepo ports.UsersRepository) *services {
	p := newProdService(prodRepo)
	t := newTransactionService(txRepo)
	u := newUserService(userRepo)
	return &services{
		prodsvc: p,
		txsvc:   t,
		userscv: u,
	}
}
func newUserService(userRepo ports.UsersRepository) *userservice {
	return &userservice{
		usersRepository: userRepo,
	}
}

func newTransactionService(txRepo ports.TransactionsRepository) *txservice {
	return &txservice{
		txRepository: txRepo,
	}
}

func newProdService(prodRepo ports.ProductsRepository) *prodservice {
	return &prodservice{
		prodRepository: prodRepo,
	}
}
