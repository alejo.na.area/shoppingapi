package services

import (
	"errors"
	"log"
	"strconv"

	"gitlab.com/shoppingapi/pkg/core/domain"
)

// product service

func (svc *services) SaveProduct(prod domain.Product) error {
	err := svc.prodsvc.prodRepository.SaveProduct(prod)
	if err != nil {
		log.Println("Error saving product")
		return err
	}
	return nil
}

func (svc *services) ShowAllProducts() ([]domain.Product, error) {

	prods, err := svc.prodsvc.prodRepository.GetAllProducts()
	if err != nil {
		log.Println("Unable to obtain products")
		return nil, err
	}
	return prods, nil
}

func (svc *services) BuyProduct(id string, id_user string) error {
	var prod domain.Product
	log.Println("Trying to buy prod")
	prod, errq := svc.prodsvc.prodRepository.GetProduct(id)
	if errq != nil {
		log.Println("Failed to get product")
		return errq
	}

	if 0 >= prod.Quantity {
		return errors.New("quantity: no stock available")
	} else {
		var op domain.Operation
		op.OP = "withdraw"
		op.Amount = prod.Price

		//checking if user has the amout to pay the product
		user_balance, erramt := svc.userscv.usersRepository.GetUserBalance(id_user)
		if erramt != nil {
			log.Println("Error while checking user balance")
			return erramt
		}

		// dont need to check for error, this values are floats and have been checked before
		fuser_balance, _ := strconv.ParseFloat(user_balance, 64)
		fprod, _ := strconv.ParseFloat(prod.Price, 64)

		if fuser_balance >= fprod {

			erru := svc.userscv.usersRepository.UpdateUserBalance(id_user, op)
			if erru != nil {
				log.Println("Error while updating user balance")
				return erru
			} else {
				errtx := svc.txsvc.txRepository.MakeTransaction(id_user, op)
				if errtx != nil {
					log.Println("Error while making transaction")
					return errtx
				}
				errprod := svc.prodsvc.prodRepository.UpdateStock(id, "-1")
				if errprod != nil {
					log.Println("Error while updating product stock")
					return errprod
				}
			}
		} else {
			log.Println("not enough balance to buy product")
			return errors.New("not enough balance")
		}
	}

	return nil
}
