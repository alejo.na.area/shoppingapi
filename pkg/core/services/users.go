package services

import (
	"log"

	jwt "github.com/golang-jwt/jwt/v5"
	"gitlab.com/shoppingapi/pkg/core/domain"
)

func (svc *services) ObtainUser(id string) (domain.User, error) {
	user, err := svc.userscv.usersRepository.GetUser(id)
	if err != nil {
		log.Println("Unable to obtain User")
		return domain.User{}, err
	}
	return user, nil
}

func (svc *services) AddUser(user domain.User) error {
	err := svc.userscv.usersRepository.SaveUser(user)
	if err != nil {
		log.Println("Imposible to save User")
		return err
	}
	return nil
}

func (svc *services) SearchUsers(q domain.QueryStruct) ([]domain.User, error) {
	users, err := svc.userscv.usersRepository.FindUsers(q)
	if err != nil {
		log.Println("Unable to obtain users with params", "with age range between", q.Minage, q.Maxage, "and gender", q.Gender, "with minum balance of:", q.Balance)
		return nil, err
	}
	return users, nil
}

func (svc *services) CheckPass(cred domain.Credentials) *jwt.Token {
	tok := svc.userscv.usersRepository.CheckPass(cred)
	if tok != nil {
		log.Println("Token confirmed")
		return tok
	}
	log.Println("unable to get token")
	return nil
}

func (svc *services) ObtainUserByMail(email string) (string, error) {
	id, err := svc.userscv.usersRepository.GetUserByMail(email)
	if err != nil {
		log.Println("Can't get user with email:", email)
		return "", err
	}
	log.Println("This user id is:", id, "with email:", email)
	return id, nil
}
