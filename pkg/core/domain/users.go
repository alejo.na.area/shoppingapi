package domain

import (
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/golang-jwt/jwt/v5"
)

// user struct type

type User struct {
	ID          string    `json:"id"`
	Name        string    `json:"name"`
	Lastname    string    `json:"lastname"`
	DateOfBirth time.Time `json:"dob"`
	Email       string    `json:"email"`
	Password    string    `json:"pw,omitempty"`
	Gender      string    `json:"gender"`
	Balance     float32   `json:"balance"`
}

func (u User) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.Password, validation.Required),
		validation.Field(&u.Lastname, validation.Required),
		validation.Field(&u.Name, validation.Required),
		validation.Field(&u.Email, is.Email, validation.Required),
		validation.Field(&u.Gender, validation.In("female", "male")),
		validation.Field(&u.Balance, is.Float),
	)
}

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type CustomClaims struct {
	Username string `json:"username"`
	ID       string `json:"id"`
	jwt.RegisteredClaims
}

type QueryStruct struct {
	Minage  string
	Maxage  string
	Gender  string
	Balance string
}

func (q QueryStruct) Validate() error {
	return validation.ValidateStruct(&q,
		validation.Field(&q.Minage, is.Int),
		validation.Field(&q.Maxage, is.Int),
		validation.Field(&q.Gender, validation.In("female", "male")),
		validation.Field(&q.Balance, is.Float),
	)
}
