package domain

import (
	"encoding/xml"
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

// transaction

type Transaction struct {
	ID     string    `json:"id"`
	UserID string    `json:"user_id"`
	Date   time.Time `json:"date"`
	Amount string    `json:"amount"`
	OP     string    `json:"operation"`
}

//transactions struct for the XML file
type Transactions struct {
	XMLName xml.Name      `xml:"transactions"`
	Tx      []Transaction `xml:"transaction"`
}

type Operation struct {
	ID     string `json:"id"`
	Amount string `json:"amount"`
	OP     string `json:"operation"`
}

func (o Operation) Validate() error {
	return validation.ValidateStruct(&o,
		validation.Field(&o.ID, is.Int),
		validation.Field(&o.Amount, validation.Required, is.Float),
		validation.Field(&o.OP, validation.Required, validation.In("deposit", "withdraw")),
	)

}
func (t Transaction) Validate() error {
	return validation.ValidateStruct(&t,
		validation.Field(&t.ID, validation.Required, is.Int),
		validation.Field(&t.UserID, validation.Required, is.Int),
		validation.Field(&t.Date, validation.Required),
		validation.Field(&t.Amount, validation.Required, is.Float),
		validation.Field(&t.OP, validation.Required, validation.In("deposit", "withdraw")),
	)

}
