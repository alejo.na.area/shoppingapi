package domain

import (
	"encoding/xml"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

// products struct

type Product struct {
	XMLName  xml.Name `json:"-" xml:"product"`
	ID       string   `json:"id" xml:"id,attr"`
	Name     string   `json:"name" xml:"name"`
	Price    string   `json:"price" xml:"price"`
	Quantity int32    `json:"qty" xml:"qty"`
}

type Products struct {
	XMLName xml.Name  `xml:"products"`
	Prods   []Product `xml:"products"`
}

func (p Product) Validate() error {
	return validation.ValidateStruct(&p,
		validation.Field(&p.Name, validation.Required),
		validation.Field(&p.Price, validation.Required, is.Float),
		validation.Field(&p.Quantity, validation.Required),
	)

}
