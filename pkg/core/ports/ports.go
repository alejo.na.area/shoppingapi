package ports

import (
	jwt "github.com/golang-jwt/jwt/v5"
	"gitlab.com/shoppingapi/pkg/core/domain"
)

//Ports -- interface

type UsersRepository interface {
	GetUser(id string) (domain.User, error)
	SaveUser(user domain.User) error
	FindUsers(query domain.QueryStruct) ([]domain.User, error)
	CheckPass(cred domain.Credentials) *jwt.Token

	GetUserByMail(email string) (string, error)
	GetUserBalance(id string) (string, error)
	UpdateUserBalance(id string, op domain.Operation) error
}

type UsersService interface {
	ObtainUser(id string) (domain.User, error)
	AddUser(user domain.User) error
	SearchUsers(query domain.QueryStruct) ([]domain.User, error)
	CheckPass(cred domain.Credentials) *jwt.Token

	ObtainUserByMail(mail string) (string, error)
}

type TransactionsRepository interface {
	GetTransactions(user string) ([]domain.Transaction, error)
	MakeTransaction(user string, deposit domain.Operation) error
}

type TransactionsService interface {
	GetTransactions(user string) ([]domain.Transaction, error)
	MakeDeposit(user string, deposit domain.Operation) error
	MakeWithdraw(user string, withdraw domain.Operation) error
}

type ProductsRepository interface {
	//Product repo
	SaveProduct(prod domain.Product) error
	GetAllProducts() ([]domain.Product, error)
	UpdateStock(id string, qty string) error
	GetProduct(id string) (domain.Product, error)
}

type ProductsService interface {
	//Product serv
	ShowAllProducts() ([]domain.Product, error)
	//AdvancedProd(query queryprod) ([]domain.Product, error) possible advanced search with queary implementation
	SaveProduct(prod domain.Product) error
	//SaveManyProducts(prods []domain.Product)
	BuyProduct(id string, id_user string) error
}
