package utils

import (
	"archive/zip"
	"encoding/xml"
	"io"
	"io/ioutil"
	"log"
	"os"
)

//zipmaker

//xmlmaker

func CreateXML(v any) ([]byte, error) {
	file, errm := xml.MarshalIndent(v, " ", "  ")
	if errm != nil {
		return nil, errm
	}
	return file, nil
}

func CreateXMLfile(name string, file []byte) (err error) {
	err = ioutil.WriteFile(name, file, 0644)
	if err != nil {
		log.Println("Error creating file")
		return err
	}
	log.Println("Created file, filename =", name)
	return nil
}

func CreateZIPfile(filename string, zipname string) error {
	//create zip
	archive, err := os.Create(zipname)
	if err != nil {
		return err
	}
	defer archive.Close()
	zipWriter := zip.NewWriter(archive)

	// opening file

	f1, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f1.Close()

	//Creating file inside the zip
	write, err := zipWriter.Create(filename)
	if err != nil {
		return err
	}
	//Writing inside the zip
	if _, err := io.Copy(write, f1); err != nil {
		return err
	}

	zipWriter.Close()
	return nil
}
