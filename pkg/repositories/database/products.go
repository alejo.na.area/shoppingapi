package database

import (
	"database/sql"
	"log"
	"strings"

	"gitlab.com/shoppingapi/pkg/core/domain"
	"gitlab.com/shoppingapi/pkg/core/ports"
)

// db product
type prodRepo struct {
	client *sql.DB
}

func NewProductRepository(c *sql.DB) ports.ProductsRepository {
	return &prodRepo{
		client: c,
	}
}

//retrive by id

//add product

//add list of products

//update stock (selling)
func (r *prodRepo) GetAllProducts() (prods []domain.Product, err error) {
	var q strings.Builder
	q.WriteString(`SELECT id, name, price, qty FROM products`)

	rows, err := r.client.Query(q.String())
	if err != nil {
		log.Println("Unable to get products")
		return nil, err
	}
	for rows.Next() {
		var p domain.Product
		err := rows.Scan(&p.ID, &p.Name, &p.Price, &p.Quantity)
		if err != nil {
			log.Println("Unable to get products")
			return nil, err
		}

		prods = append(prods, p)
	}
	return
}

func (r *prodRepo) SaveProduct(prod domain.Product) error {
	var q strings.Builder
	log.Println("Trying to save product", prod.Name)
	q.WriteString("INSERT INTO products (name, price, qty) VALUES ($1 , $2, $3)")
	_, err := r.client.Exec(q.String(), prod.Name, prod.Price, prod.Quantity)

	if err != nil {
		log.Println("Failed save product", prod.Name)
		return err
	}
	return nil
}

func (r *prodRepo) UpdateStock(id string, qty string) error {
	var q strings.Builder
	log.Println("Updating stock")
	q.WriteString("UPDATE products set qty = (qty + $1) WHERE id = $2")
	_, err := r.client.Exec(q.String(), qty, id)
	if err != nil {
		log.Println("Error updating stock")
	}
	return nil
}

func (r *prodRepo) GetProduct(id string) (prod domain.Product, err error) {
	var q strings.Builder
	log.Println("Trying to get product info")
	q.WriteString("SELECT id, name, price, qty FROM products WHERE id = $1")
	row := r.client.QueryRow(q.String(), id)
	err = row.Scan(&prod.ID, &prod.Name, &prod.Price, &prod.Quantity)
	if err != nil {
		log.Println("Failed to retrive stock quantity from product id:", id)
		return prod, err
	}

	return prod, nil
}
