package database

import (
	"database/sql"
	"log"
	"strings"
	"time"

	"gitlab.com/shoppingapi/pkg/core/domain"
	"gitlab.com/shoppingapi/pkg/core/ports"
)

type txRepo struct {
	client *sql.DB
}

func NewTransactionRepository(c *sql.DB) ports.TransactionsRepository {
	return &txRepo{
		client: c,
	}
}

func (r *txRepo) GetTransactions(user string) (transactions []domain.Transaction, err error) {
	var q strings.Builder
	log.Println("Getting transactions, from user:", user)
	q.WriteString("SELECT * FROM transactions where user_id = '")
	q.WriteString(user)
	q.WriteString("'")
	rows, err := r.client.Query(q.String())
	if err != nil {
		log.Println("Unable to get transactions")
		return nil, err
	}
	for rows.Next() {
		var t domain.Transaction
		err := rows.Scan(&t.ID, &t.UserID, &t.Date, &t.Amount, &t.OP)
		if err != nil {
			log.Println("Unable to get transactions")
			return nil, err
		}

		transactions = append(transactions, t)
	}
	return

}

func (r *txRepo) MakeTransaction(id string, operation domain.Operation) error {
	var q strings.Builder
	date := time.Now()

	q.WriteString("INSERT INTO transactions (user_id, date, amount, operation) VALUES ($1, $2 , $3, $4)")
	_, err := r.client.Exec(q.String(), id, date, operation.Amount, operation.OP)

	if err != nil {
		log.Println("Failed to make", operation.OP)
		return err
	}
	return nil
}
