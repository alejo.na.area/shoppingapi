package database

import (
	"database/sql"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/shoppingapi/pkg/core/domain"
	"gitlab.com/shoppingapi/pkg/core/ports"
	"golang.org/x/crypto/bcrypt"
)

type userRepo struct {
	client *sql.DB
}

func NewUserRepository(c *sql.DB) ports.UsersRepository {
	return &userRepo{
		client: c,
	}
}

func (r *userRepo) GetUser(id string) (user domain.User, err error) {
	var q strings.Builder
	q.WriteString(`SELECT id, name, lastname, dob, email, gender, balance FROM users WHERE id = $1`)

	row := r.client.QueryRow(q.String(), id)
	err = row.Scan(&user.ID, &user.Name, &user.Lastname, &user.DateOfBirth, &user.Email, &user.Gender, &user.Balance)
	if err != nil {
		log.Println("Unable to get user")
		return domain.User{}, err
	}
	log.Println("User found, ID:", user.ID)
	return
}

func (r *userRepo) SaveUser(user domain.User) error {
	var q strings.Builder
	q.WriteString(`INSERT INTO users (id, name, lastname, dob, email, pw, gender, balance) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`)

	bytes, errb := bcrypt.GenerateFromPassword([]byte(user.Password), 4)
	if errb != nil {
		return errb
	}

	_, err := r.client.Exec(q.String(), user.ID, user.Name, user.Lastname, user.DateOfBirth, user.Email, bytes, user.Gender, user.Balance)
	if err != nil {
		log.Println("Unable to save user")
		return err
	} else {
		log.Println("User saved, id: ", user.ID)
	}

	return nil
}

func (r *userRepo) FindUsers(query domain.QueryStruct) (users []domain.User, err error) {
	var q strings.Builder
	log.Println("Finding Users")
	q.WriteString(`SELECT id, name, lastname, dob, gender, email, balance FROM users WHERE `)
	//creating the query based on the given parameters

	layout := "January 2006"
	now := time.Now()
	now.Year()

	age1, _ := strconv.Atoi(query.Minage)
	st1 := now.Month().String() + " " + strconv.Itoa(now.Year()-age1)
	t1, _ := time.Parse(layout, st1)
	st1 = t1.Format("2006-01-02 15:04:05")

	age2, _ := strconv.Atoi(query.Maxage)
	st2 := now.Month().String() + " " + strconv.Itoa(now.Year()-age2)
	t2, _ := time.Parse(layout, st2)
	st2 = t2.Format("2006-01-02 15:04:05")

	//t1 its for "youngest" dob and t2 is for the "oldest"

	if query.Maxage != "" {
		q.WriteString("dob BETWEEN '")
		q.WriteString(st2)
		q.WriteString("'")
	} else {
		q.WriteString("dob BETWEEN '1900-01-01 00:00:00'")
	}
	if query.Minage != "" {
		q.WriteString(" AND '")
		q.WriteString(st1)
		q.WriteString("'")
	} else {
		q.WriteString(" AND '2100-01-01 00:00:00'")
	}
	if query.Gender != "" {
		q.WriteString(" AND gender = '")
		q.WriteString(query.Gender)
		q.WriteString("'")
	}
	if query.Balance != "" {
		q.WriteString(" AND balance >= ")
		q.WriteString(query.Balance)
	} else {
		q.WriteString(" AND balance >= 0")
	}
	//Future values can be added here with another if sentence

	rows, err := r.client.Query(q.String())
	if err != nil {
		log.Println("Unable to get users")
		return nil, err
	}
	for rows.Next() {
		var u domain.User
		err := rows.Scan(&u.ID, &u.Name, &u.Lastname, &u.DateOfBirth, &u.Gender, &u.Email, &u.Balance)
		if err != nil {
			log.Println("Unable to get user")
			return nil, err
		}

		users = append(users, u)
	}
	return

}

func (r *userRepo) CheckPass(cred domain.Credentials) *jwt.Token {
	var q strings.Builder
	var st, id string

	log.Println("Checking pw in the db")
	q.WriteString("SELECT pw,id FROM users WHERE email = '")
	q.WriteString(cred.Username)
	q.WriteString("'")
	row := r.client.QueryRow(q.String())
	err1 := row.Scan(&st, &id)
	if err1 != nil {
		log.Println("Unable to get pw", err1.Error())
		return nil
	}
	err := bcrypt.CompareHashAndPassword([]byte(st), []byte(cred.Password))
	if err != nil {
		log.Println("Error wrong password")
		return nil
	}
	log.Println("creating token")
	expirationTime := time.Now().Add(60 * time.Minute)
	claims := &domain.CustomClaims{
		Username: cred.Username,
		ID:       id,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expirationTime),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token.Valid = true
	return token
}

func (r *userRepo) GetUserByMail(email string) (string, error) {
	var id string
	var q strings.Builder
	//getting user id
	q.WriteString("SELECT id FROM users where email = $1")
	row := r.client.QueryRow(q.String(), email)
	err := row.Scan(&id)
	if err != nil {
		log.Println("Failed to get id while doing a deposit")
		return "", err
	}
	return id, nil
}

func (r *userRepo) GetUserBalance(id string) (string, error) {
	var q strings.Builder
	var balance string

	q.WriteString("SELECT balance FROM users where id = $1")
	row := r.client.QueryRow(q.String(), id)
	err := row.Scan(&balance)
	if err != nil {
		log.Println("Failed to retrive balance from user_id:", id)
		return "", nil
	}
	return balance, nil
}

func (r *userRepo) UpdateUserBalance(id string, op domain.Operation) error {
	var q strings.Builder

	if op.OP == "deposit" {
		log.Println("Changing balance in user account: + $", op.Amount)
		q.WriteString("UPDATE users set balance = (balance + $1) where id = $2")
	} else {
		log.Println("Changing balance in user account: - $", op.Amount)
		q.WriteString("UPDATE users set balance = (balance - $1) where id = $2")
	}

	_, err := r.client.Exec(q.String(), op.Amount, id)

	if err != nil {
		log.Println("Failed to change balance in user table")
		return err
	}
	return nil
}
