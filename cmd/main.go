package main

import (
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"gitlab.com/shoppingapi/db"
	"gitlab.com/shoppingapi/pkg/core/services"
	"gitlab.com/shoppingapi/pkg/handlers"
	"gitlab.com/shoppingapi/pkg/repositories/database"
)

func main() {

	db, err := db.NewRelationalDB("postgres://shopping:123123123@shopping.db:5432/shopping?sslmode=disable")
	if err != nil {
		panic(err)
	}
	//repo
	userRepo := database.NewUserRepository(db)
	txRepo := database.NewTransactionRepository(db)
	prodRepo := database.NewProductRepository(db)
	//service

	service := services.NewService(prodRepo, txRepo, userRepo)

	//HTTP Handler
	handler := handlers.NewHTTPHandler(service, service, service)
	//router
	router := gin.New()
	//endpoints

	//testing auth
	router.GET("home/", handler.Home)

	router.POST("users/", handler.Add)
	router.GET("/users/:id", handler.GetUser)
	router.GET("/users/search", handler.SearchUsers)
	router.POST("/users/login", handler.Checkpw)
	router.GET("/users/status", handler.TokenStatus)

	//transactions

	router.GET("/users/transactions", handler.GetTransactions)
	router.POST("/users/transactions/deposit", handler.Deposit)
	router.POST("/users/transactions/withdraw", handler.Withdraw)

	//products

	router.GET("/products", handler.GetAllProducts)
	router.POST("/products", handler.UploadProduct)
	router.GET("/products/buy/", handler.BuyProduct)

	//Create an endpoint that generates a XLM file that has all the products
	router.GET("/products/show", handler.GetProductsXLM)
	router.GET("/transactions/file", handler.GetUserTransactionsXML)

	router.Run(":8080")
}
